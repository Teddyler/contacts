//
//  DisplayPhoneEmail.h
//  Contacts
//
//  Created by Tyler Barnes on 2/24/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Messages/Messages.h>
#import <MessageUI/MessageUI.h>

@interface DisplayPhoneEmail : UITableViewCell <MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UIButton *phone;
@property (strong, nonatomic) IBOutlet UIButton *email;

@property (strong, nonatomic) UIAlertController* alert;

- (IBAction)callText:(UIButton *)sender;
- (IBAction)email:(UIButton *)sender;

@end
