//
//  FirstLastName.m
//  Contacts
//
//  Created by Tyler Barnes on 2/23/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import "FirstLastName.h"

@implementation FirstLastName

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _firstName.delegate = self;
    _lastName.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
