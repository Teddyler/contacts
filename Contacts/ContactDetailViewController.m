//
//  ContactDetailViewController.m
//  Contacts
//
//  Created by Tyler Barnes on 2/23/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import "ContactDetailViewController.h"

@interface ContactDetailViewController ()


@end

@implementation ContactDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _contactInfoTable.allowsSelection = NO;

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (!cell) {
        switch (indexPath.row) {
            case 0:
                [tableView registerNib:[UINib nibWithNibName:@"FirstLastName" bundle:nil] forCellReuseIdentifier:@"firstLastName"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"firstLastName"];
                break;
            case 1:
                [tableView registerNib:[UINib nibWithNibName:@"PhoneEmail" bundle:nil] forCellReuseIdentifier:@"phoneEmail"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"phoneEmail"];
                break;
            case 2:
                [tableView registerNib:[UINib nibWithNibName:@"Address1" bundle:nil] forCellReuseIdentifier:@"street1"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"street1"];
                break;
            case 3:
                [tableView registerNib:[UINib nibWithNibName:@"Address2" bundle:nil] forCellReuseIdentifier:@"street2"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"street2"];
                break;
            case 4:
                [tableView registerNib:[UINib nibWithNibName:@"CityStateZip" bundle:nil] forCellReuseIdentifier:@"cityStateZip"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"cityStateZip"];
                break;
                
            default:
                break;
        }
    }
    
    return cell;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if(![touch.view isMemberOfClass:[UITextField class]]) {
        [touch.view endEditing:YES];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    ContactDetailViewController * cdvc = segue.destinationViewController;
    
    if([segue.identifier  isEqual:@"editCont"]){
        cdvc.contact = self.contact;
    }
}

- (IBAction)saveContact:(UIBarButtonItem *)sender {
    
    AppDelegate* appD = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = appD.persistentContainer.viewContext;
    
    if (self.contact) {
//        [self.contact setValue:<#(nullable id)#> forKey:@"firstName"];
//        [self.contact setValue:<#(nullable id)#> forKey:@"lastName"];
//        [self.contact setValue:<#(nullable id)#> forKey:@"phone"];
//        [self.contact setValue:<#(nullable id)#> forKey:@"email"];
//        [self.contact setValue:<#(nullable id)#> forKey:@"street1"];
//        [self.contact setValue:<#(nullable id)#> forKey:@"street2"];
//        [self.contact setValue:<#(nullable id)#> forKey:@"city"];
//        [self.contact setValue:<#(nullable id)#> forKey:@"state"];
//        [self.contact setValue:<#(nullable id)#> forKey:@"zip"];
//        [self.contact setValue:[NSString stringWithFormat:@"%@, %@, %@, %@ %@", cell.street1.text, cell.street2.text, cell.city.text, cell.state.text, cell.zip.text] forKey:@"location"];
        
        //Transition to Display Contact
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        DisplayContactViewController *dcvc = [storyboard instantiateViewControllerWithIdentifier:@"DisplayContactViewController"];
        
        UINavigationController *navController = self.navigationController;
        //Get all view controllers in navigation controller currently
        NSMutableArray *controllers=[[NSMutableArray alloc] initWithArray:navController.viewControllers] ;
        //Remove the last view controller
        [controllers removeLastObject];
        [controllers removeLastObject];
        //set the new set of view controllers
        [navController setViewControllers:controllers];
        dcvc.contact = self.contact;
        //Push a new view controller
        [navController pushViewController:dcvc animated:NO];
    }
    else{
        NSManagedObject * newContact = [NSEntityDescription insertNewObjectForEntityForName:@"Contact" inManagedObjectContext:context];
        
//        [newContact setValue:<#(nullable id)#> forKey:@"firstName"];
//        [newContact setValue:<#(nullable id)#> forKey:@"lastName"];
//        [newContact setValue:<#(nullable id)#> forKey:@"phone"];
//        [newContact setValue:<#(nullable id)#> forKey:@"email"];
//        [newContact setValue:<#(nullable id)#> forKey:@"street1"];
//        [newContact setValue:<#(nullable id)#> forKey:@"street2"];
//        [newContact setValue:<#(nullable id)#> forKey:@"city"];
//        [newContact setValue:<#(nullable id)#> forKey:@"state"];
//        [newContact setValue:<#(nullable id)#> forKey:@"zip"];
//        [newContact setValue:[NSString stringWithFormat:@"%@, %@, %@, %@, %@", cell.street1.text, cell.street2.text, cell.city.text, cell.state.text, cell.zip.text] forKey:@"location"];
        [newContact setValue:@"Tyler" forKey:@"firstName"];
        [newContact setValue:@"Tyler" forKey:@"lastName"];
        [newContact setValue:[NSNumber numberWithInteger: 8018828350] forKey:@"phone"];
        [newContact setValue:@"Tyler" forKey:@"email"];
        [newContact setValue:@"Tyler" forKey:@"street1"];
        [newContact setValue:@"Tyler" forKey:@"street2"];
        [newContact setValue:@"Tyler" forKey:@"city"];
        [newContact setValue:@"Tyler" forKey:@"state"];
        [newContact setValue:@"Tyler" forKey:@"zip"];
        [newContact setValue:@"619 W 225 S, Layton, UT, 84041" forKey:@"location"];
        
        //Transition to Display Contact
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        DisplayContactViewController *dcvc = [storyboard instantiateViewControllerWithIdentifier:@"DisplayContactViewController"];

        UINavigationController *navController = self.navigationController;
        //Get all view controllers in navigation controller currently
        NSMutableArray *controllers=[[NSMutableArray alloc] initWithArray:navController.viewControllers] ;
        //Remove the last view controller
        [controllers removeLastObject];
        //set the new set of view controllers
        [navController setViewControllers:controllers];
        dcvc.contact = newContact;
        //Push a new view controller
        [navController pushViewController:dcvc animated:NO];
    }
}
@end
