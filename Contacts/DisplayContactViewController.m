//
//  DisplayContactViewController.m
//  Contacts
//
//  Created by Tyler Barnes on 2/24/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import "DisplayContactViewController.h"

@interface DisplayContactViewController ()

@end

@implementation DisplayContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _contactTable.allowsSelection = NO;

    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:[self.contact valueForKey:@"location"]
                 completionHandler:^(NSArray* placemarks, NSError* error){
                     if (error) {
                         NSLog(@"%@", error);
                     } else {
                         CLPlacemark *placemark = [placemarks lastObject];
                         float spanX = 0.00725;
                         float spanY = 0.00725;
                         MKCoordinateRegion region;
                         region.center.latitude = placemark.location.coordinate.latitude;
                         region.center.longitude = placemark.location.coordinate.longitude;
                         region.span = MKCoordinateSpanMake(spanX, spanY);
                         [_map setRegion:region animated:YES];
                     }
                 }
     ];
    
    [_map setScrollEnabled:NO];
    [_map setZoomEnabled:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (!cell) {
        switch (indexPath.row) {
            case 0:
                [tableView registerNib:[UINib nibWithNibName:@"DisplayName" bundle:nil] forCellReuseIdentifier:@"firstLastName"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"firstLastName"];
                break;
            case 1:
                [tableView registerNib:[UINib nibWithNibName:@"DisplayPhoneEmail" bundle:nil] forCellReuseIdentifier:@"phoneEmail"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"phoneEmail"];
                break;
            case 2:
                [tableView registerNib:[UINib nibWithNibName:@"DisplayStreet1" bundle:nil] forCellReuseIdentifier:@"street1"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"street1"];
                break;
            case 3:
                [tableView registerNib:[UINib nibWithNibName:@"DisplayStreet2" bundle:nil] forCellReuseIdentifier:@"street2"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"street2"];
                break;
            case 4:
                [tableView registerNib:[UINib nibWithNibName:@"DisplayCityStateZip" bundle:nil] forCellReuseIdentifier:@"cityStateZip"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"cityStateZip"];
                break;
                
            default:
                break;
        }
    }
    
    return cell;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    ContactDetailViewController * cdvc = segue.destinationViewController;
    
    if([segue.identifier  isEqual:@"oldCont"]){
        cdvc.contact = _contact;
    }
}

@end
