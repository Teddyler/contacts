//
//  DisplayContactViewController.h
//  Contacts
//
//  Created by Tyler Barnes on 2/24/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "ContactDetailViewController.h"

@interface DisplayContactViewController : UIViewController <MKMapViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *contactTable;

@property (strong, nonatomic) NSManagedObject* contact;

@property (strong, nonatomic) IBOutlet MKMapView *map;

@end
