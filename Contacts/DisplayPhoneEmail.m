//
//  DisplayPhoneEmail.m
//  Contacts
//
//  Created by Tyler Barnes on 2/24/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import "DisplayPhoneEmail.h"

@implementation DisplayPhoneEmail

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)callText:(UIButton *)sender {
    
    _alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* callButton = [UIAlertAction actionWithTitle:@"CALL"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           NSString *phoneNumber = [@"tel://" stringByAppendingString:_phone.titleLabel.text];
                                                           NSDictionary *options = @{UIApplicationOpenURLOptionUniversalLinksOnly : @NO};
                                                           [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber] options: options completionHandler:nil];
                                                           
                                                       }];
    
    UIAlertAction* textButton = [UIAlertAction actionWithTitle:@"TEXT"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           MFMessageComposeViewController * text = [[MFMessageComposeViewController alloc] init];
                                                           text.messageComposeDelegate = self;
                                                           
                                                           [text setRecipients:[NSArray arrayWithObjects:_phone.titleLabel.text, nil]];
                                                       }];
    
    [_alert addAction:callButton];
    [_alert addAction:textButton];
}

- (IBAction)email:(UIButton *)sender {
    
    
    
}
@end
