//
//  ContactDetailViewController.h
//  Contacts
//
//  Created by Tyler Barnes on 2/23/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "FirstLastName.h"
#import "PhoneEmail.h"

#import "DisplayContactViewController.h"

@interface ContactDetailViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *contactInfoTable;

@property (strong, nonatomic) IBOutlet MKMapView *map;

@property (strong, nonatomic) NSManagedObject* contact;

- (IBAction)saveContact:(UIBarButtonItem *)sender;


@end
