//
//  DisplayName.h
//  Contacts
//
//  Created by Tyler Barnes on 2/24/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DisplayName : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *firstName;
@property (strong, nonatomic) IBOutlet UILabel *lastName;

@end
