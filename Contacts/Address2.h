//
//  Address2.h
//  Contacts
//
//  Created by Tyler Barnes on 2/23/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Address2 : UITableViewCell <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *street2;

@end
