//
//  DisplayCityStateZip.h
//  Contacts
//
//  Created by Tyler Barnes on 2/24/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DisplayCityStateZip : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *city;
@property (strong, nonatomic) IBOutlet UILabel *state;
@property (strong, nonatomic) IBOutlet UILabel *zip;


@end
