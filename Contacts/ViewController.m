//
//  ViewController.m
//  Contacts
//
//  Created by Tyler Barnes on 2/23/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _contactsTable.dataSource = self;
    _contactsTable.delegate = self;
    
    _contactsTable.allowsMultipleSelectionDuringEditing = NO;
    
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
    [self.view addSubview:_contactsTable];

}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self loadData];
}

-(void)loadData {
    
    AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
    NSFetchRequest * request = [[NSFetchRequest alloc] initWithEntityName:@"Contact"];
    NSArray* array = [[context executeFetchRequest:request error:nil] mutableCopy];
    
    _contactsArray = [array mutableCopy];
    
    [_contactsTable reloadData];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _contactsArray.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    NSManagedObject* object = _contactsArray[indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", [object valueForKey:@"firstName"], [object valueForKey:@"lastName"]];
    
    return cell;
    
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewCellEditingStyleDelete;
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    NSIndexPath *indexPath = [_contactsTable indexPathForCell:sender];
    ContactDetailViewController * cdvc = segue.destinationViewController;
    
    if([segue.identifier  isEqual:@"oldCont"]){
        cdvc.contact = _contactsArray[indexPath.row];
    }
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated{
    [super setEditing:editing animated:animated];
    
    _toolbarButtons = [self.toolbarItems mutableCopy];
    
    [_contactsTable setEditing:editing animated:YES];
    
    if (editing) {
        _addButton.enabled = NO;
    } else {
        _addButton.enabled = YES;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // If row is deleted, remove it from the list.
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObject * deletingObject = _contactsArray[indexPath.row];
        NSManagedObjectContext *context = [deletingObject managedObjectContext];
        [context deleteObject:deletingObject];
        
        NSError *error;
        if (![context save:&error]) {
            NSLog(@"Object Failed to Delete!");
        }
        
        //        [self loadData];
        [UIView transitionWithView: _contactsTable duration: 0.35f options: UIViewAnimationOptionTransitionCrossDissolve animations: ^(void){
            [self loadData];
            if (_contactsArray.count == 0) {
                [tableView setEditing:NO animated:YES];
            }
        } completion:nil ];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
