//
//  DisplayStreet2.h
//  Contacts
//
//  Created by Tyler Barnes on 2/24/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DisplayStreet2 : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *street2;

@end
