//
//  Address1.m
//  Contacts
//
//  Created by Tyler Barnes on 2/23/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import "Address1.h"

@implementation Address1

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _street1.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
