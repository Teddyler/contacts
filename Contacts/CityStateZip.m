//
//  CityStateZip.m
//  Contacts
//
//  Created by Tyler Barnes on 2/23/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import "CityStateZip.h"

@implementation CityStateZip

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _city.delegate = self;
    _state.delegate = self;
    _zip.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
